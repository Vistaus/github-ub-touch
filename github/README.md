# github (unofficial)

<h1 align="center">
  <br>
  <img src="img/logo.svg" width="256px" alt="github-web">
  <br>
  <br>
  Unofficial github web app. The license only refers to the web container. Rights to the content (including logos and brands) belong to their owners. No warranty.
  <br>
  <br>
</h1>

https://github.com/

github-ub-touch is an unofficial github web client for Ubuntu Touch OS based on Webkit

## Credits
Based on Code by Rudi Timmermans: https://gitlab.com/Xray2000/sailbook
and Stefan Kalb: https://github.com/stekalb/yatwitter-webcontainer

## License

GNU General Public License v3.0

